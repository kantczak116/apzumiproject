package com.core.data.repository.github.owner

import com.google.gson.annotations.SerializedName

data class GithubRepositoryOwner(@SerializedName("login") val login: String,
                                 @SerializedName("avatar_url") val avatarUrl: String)