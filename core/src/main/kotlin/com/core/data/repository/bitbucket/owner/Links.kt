package com.core.data.repository.bitbucket.owner

import com.google.gson.annotations.SerializedName

data class Links(@SerializedName("avatar") val avatar: Avatar)