package com.core.data.repository.bitbucket.owner

import com.google.gson.annotations.SerializedName

data class Avatar(@SerializedName("href") val href: String)