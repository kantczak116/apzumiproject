package com.core.data.repository

import com.core.data.repository.bitbucket.owner.BitbucketRepositoryOwner
import com.core.data.repository.github.owner.GithubRepositoryOwner
import com.google.gson.annotations.SerializedName

sealed class Repository {

    data class BitbucketRepository(@SerializedName("owner") val owner: BitbucketRepositoryOwner,
                                   @SerializedName("name") val name: String,
                                   @SerializedName("description") val description: String) : Repository()

    data class GithubRepository(@SerializedName("owner") val owner: GithubRepositoryOwner,
                                @SerializedName("description") val description: String,
                                @SerializedName("full_name") val name: String) : Repository()

}