package com.core.data.repository.bitbucket.owner

import com.google.gson.annotations.SerializedName

data class BitbucketRepositoryOwner(@SerializedName("username") val username: String,
                                    @SerializedName("display_name") val displayName: String,
                                    @SerializedName("links") val links: Links)