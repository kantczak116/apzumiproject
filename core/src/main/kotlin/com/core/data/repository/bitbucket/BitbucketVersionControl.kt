package com.core.data.repository.bitbucket

import com.core.data.repository.Repository
import com.google.gson.annotations.SerializedName

data class BitbucketVersionControl(@SerializedName("values") val repositoryList: List<Repository.BitbucketRepository>)