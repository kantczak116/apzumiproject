package com.core.data.repository

object RepositoryType {
    const val BITBUCKET = 1
    const val GITHUB = 2
}