package com.core.network.retrofit.repository

import com.core.data.repository.bitbucket.BitbucketVersionControl
import io.reactivex.Single
import retrofit2.http.GET

interface RetrofitBitbucketRepository {

    @get:GET("?fields=values.name,values.owner,values.description")
    val repositories : Single<BitbucketVersionControl>

}