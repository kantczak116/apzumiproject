package com.core.network.retrofit.service

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitServiceProvider(private val url: String,
                              private val httpClient: OkHttpClient) : HttpServiceProvider {

    init {
        createRetrofit()
    }

    lateinit var retrofit: Retrofit

    private fun createRetrofit() {
        val converterFactory = getGsonFactory()
        val callAdapterFactory = getRxJavaFactory()
        val httpClient = addLoggingInterceptor()

        retrofit = Retrofit.Builder()
                .baseUrl(url)
                .client(httpClient)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(callAdapterFactory)
                .build()
    }

    private fun addLoggingInterceptor(): OkHttpClient {
        return httpClient.newBuilder()
                .addInterceptor(loggingInterceptor())
                .build()
    }

    private fun loggingInterceptor(): Interceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return loggingInterceptor
    }

    private fun getGsonFactory(): GsonConverterFactory {
        return GsonBuilder().setLenient()
                .create()
                .let {
                    GsonConverterFactory.create(it)
                }
    }

    private fun getRxJavaFactory(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.create()
    }

    override fun <T> getService(serviceClass: Class<T>): T {
        return retrofit.create(serviceClass)
    }
}