package com.core.network.retrofit.repository

import com.core.data.repository.Repository
import com.core.network.GithubService
import com.core.network.retrofit.service.HttpServiceProvider
import io.reactivex.Single

class ApiGithubRepository(serviceProvider: HttpServiceProvider) : GithubService {

    private val service = serviceProvider.getService(RetrofitGithubRepository::class.java)

    override val repositories: Single<List<Repository.GithubRepository>>
        get() = service.repositories
}