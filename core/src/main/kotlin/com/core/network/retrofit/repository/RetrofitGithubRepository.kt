package com.core.network.retrofit.repository

import com.core.data.repository.Repository
import io.reactivex.Single
import retrofit2.http.GET

interface RetrofitGithubRepository {

    @get:GET("/repositories")
    val repositories: Single<List<Repository.GithubRepository>>
}