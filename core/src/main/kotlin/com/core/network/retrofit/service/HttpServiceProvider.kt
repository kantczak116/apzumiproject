package com.core.network.retrofit.service

interface HttpServiceProvider {

    fun <T> getService(serviceClass: Class<T>): T
}