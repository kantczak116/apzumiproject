package com.core.network.retrofit.repository

import com.core.data.repository.bitbucket.BitbucketVersionControl
import com.core.network.BitbucketService
import com.core.network.retrofit.service.HttpServiceProvider
import io.reactivex.Single

class ApiBitbucketRepository(serviceProvider: HttpServiceProvider) : BitbucketService {

    private val service = serviceProvider.getService(RetrofitBitbucketRepository::class.java)

    override val repositories: Single<BitbucketVersionControl>
        get() = service.repositories
}