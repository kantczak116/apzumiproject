package com.core.network

import com.core.data.repository.Repository
import io.reactivex.Single

interface GithubService {

    val repositories: Single<List<Repository.GithubRepository>>
}