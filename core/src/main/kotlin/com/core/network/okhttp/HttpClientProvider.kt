package com.core.network.okhttp

import okhttp3.OkHttpClient

class HttpClientProvider(private val timeout: Timeout) {

    val httpClient: OkHttpClient

    init {
        httpClient = buildOkHttpClient()
    }

    private fun buildOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .connectTimeout(timeout.time, timeout.unit)
                .build()
    }
}