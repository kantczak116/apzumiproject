package com.core.network.okhttp

import java.util.concurrent.TimeUnit

class Timeout(val time: Long,
              val unit: TimeUnit)
