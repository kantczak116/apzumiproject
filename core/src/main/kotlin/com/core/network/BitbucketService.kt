package com.core.network

import com.core.data.repository.bitbucket.BitbucketVersionControl
import io.reactivex.Single

interface BitbucketService {

    val repositories: Single<BitbucketVersionControl>
}