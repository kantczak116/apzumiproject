package com.core.utils

import com.core.data.repository.Repository
import java.util.*

class RepositoryComparator {

    fun compare(repositories: List<Repository>) {
        Collections.sort(repositories, { o1: Repository, o2: Repository ->
            when (o1) {
                is Repository.BitbucketRepository -> compareBitbucketRepository(o1, o2)
                is Repository.GithubRepository -> compareGithubRepository(o1, o2)
            }
        })
    }

    private fun compareGithubRepository(o1: Repository.GithubRepository, o2: Repository): Int {
        return when (o2) {
            is Repository.BitbucketRepository -> o1.name.compareTo(o2.name, true)
            is Repository.GithubRepository -> o1.name.compareTo(o2.name, true)
        }
    }

    private fun compareBitbucketRepository(o1: Repository.BitbucketRepository, o2: Repository): Int {
        return when (o2) {
            is Repository.BitbucketRepository -> o1.name.compareTo(o2.name, true)
            is Repository.GithubRepository -> o1.name.compareTo(o2.name, true)
        }
    }
}