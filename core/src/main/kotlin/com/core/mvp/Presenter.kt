package com.core.mvp

interface Presenter<in V : View> {

    fun attachView(view: V)

    fun detachView()
}