package com.core.mvp

abstract class BasePresenter<V : View> : Presenter<V> {

    protected var view: V? = null
    private var firstAttach = true

    override fun attachView(view: V) {
        this.view = view

        if (firstAttach) {
            startPresenting()
            firstAttach = false
        }
    }

    /**
     * Override in specific presenter to call it's body only once on #attachView
     */
    open protected fun startPresenting() {}

    override fun detachView() {
        view = null
    }
}