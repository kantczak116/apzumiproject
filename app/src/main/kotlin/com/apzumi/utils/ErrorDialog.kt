package com.apzumi.utils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.apzumi.R

class ErrorDialog(private val context: Context) {

    private val dialogBuilder = AlertDialog.Builder(context)

    fun show(onClickListener: DialogInterface.OnClickListener) {
        val dialog = initializeDialog(onClickListener)
        dialog.show()
    }

    private fun initializeDialog(onTryAgainClickListener: DialogInterface.OnClickListener): AlertDialog {
        val alertDialog = when (isConnected()) {
            true -> initializeUnknownErrorDialog()
            false -> initializeInternetErrorDialog()
        }
        return alertDialog
                .setCancelable(false)
                .setPositiveButton(R.string.try_again, onTryAgainClickListener)
                .create()
    }

    private fun initializeInternetErrorDialog(): AlertDialog.Builder {
        return dialogBuilder.setTitle(R.string.internet_not_available)
                .setMessage(R.string.turn_on_internet)
    }

    private fun initializeUnknownErrorDialog(): AlertDialog.Builder {
        return dialogBuilder.setTitle(R.string.ups)
                .setMessage(R.string.unknown_error)
    }

    private fun isConnected(): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }
}