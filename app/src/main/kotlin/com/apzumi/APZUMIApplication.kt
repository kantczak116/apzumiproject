package com.apzumi

import android.app.Application
import com.apzumi.di.component.AppComponent
import com.apzumi.di.component.DaggerAppComponent
import com.apzumi.di.modules.AppModule

class APZUMIApplication : Application() {

    companion object {
        val component: AppComponent by lazy {
            DaggerAppComponent.builder()
                    .appModule(AppModule())
                    .build()
        }
    }

}