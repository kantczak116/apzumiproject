package com.apzumi.activity.main

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.ImageView
import com.apzumi.APZUMIApplication
import com.apzumi.R
import com.apzumi.activity.PresentationActivity
import com.apzumi.activity.details.DetailsActivity
import com.apzumi.adapter.OnRepositoryItemClickListener
import com.apzumi.adapter.RepositoriesListAdapter
import com.apzumi.presenter.MainActivityPresenter
import com.apzumi.utils.ErrorDialog
import com.core.data.repository.Repository
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

const val IMAGE_TRANSITION_NAME_KEY = "imageTransitionKey"

class MainActivity : PresentationActivity<MainView>(), MainView, OnRepositoryItemClickListener {

    @Inject
    lateinit var presenter: MainActivityPresenter

    private lateinit var repositoriesListAdapter: RepositoriesListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setPresenter(presenter, this)
        setContentView(R.layout.activity_main)
        initializeRepositoriesList()
        initializeSortCheckBox()
    }

    private fun initializeSortCheckBox() {
        sort_check_box.setOnCheckedChangeListener { _, isChecked ->
            presenter.onSortCheckboxClicked(isChecked)
        }
    }

    private fun initializeRepositoriesList() {
        repositoriesListAdapter = RepositoriesListAdapter(baseContext, ArrayList(), this)
        val linearLayoutManager = LinearLayoutManager(this)
        setting_menu_list.apply {
            layoutManager = linearLayoutManager
            itemAnimator = DefaultItemAnimator()
            adapter = repositoriesListAdapter
        }
    }

    override fun injectComponent() {
        APZUMIApplication.component
                .inject(this)
    }

    override fun showError() {
        ErrorDialog(this).show(DialogInterface.OnClickListener { _, _ ->
            presenter.fetchRepositories()
        })
    }

    override fun showProgressBar() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progress_bar.visibility = View.GONE
    }

    override fun showRepositoryList(repositoryList: List<Repository>) {
        sort_check_box.isChecked = false
        repositoriesListAdapter.setList(repositoryList)
    }

    override fun onClick(imageView: ImageView, item: Repository) {
        presenter.onRepositoryItemClicked(item)
        showRepositoryDetailsScreen(imageView)
    }

    private fun showRepositoryDetailsScreen(imageView: ImageView) {
        val transitionName = ViewCompat.getTransitionName(imageView)
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(IMAGE_TRANSITION_NAME_KEY, transitionName)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                imageView,
                transitionName)
        startActivity(intent, options.toBundle())
    }

    override fun showSortedRepositoryList(sortedList: ArrayList<Repository>) {
        sort_check_box.isChecked = true
        repositoriesListAdapter.setList(sortedList)
    }
}