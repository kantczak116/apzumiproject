package com.apzumi.activity.main

import com.core.data.repository.Repository
import com.core.mvp.View

interface MainView : View {

    fun showRepositoryList(repositoryList: List<Repository>)

    fun showError()

    fun showProgressBar()

    fun hideProgressBar()

    fun showSortedRepositoryList(sortedList: ArrayList<Repository>)
}