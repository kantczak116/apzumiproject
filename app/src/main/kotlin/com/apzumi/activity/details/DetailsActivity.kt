package com.apzumi.activity.details

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import com.apzumi.APZUMIApplication
import com.apzumi.R
import com.apzumi.activity.PresentationActivity
import com.apzumi.presenter.DetailsActivityPresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.activity_details.*
import javax.inject.Inject
import android.support.v4.view.ViewCompat.setTransitionName
import android.os.Build
import com.apzumi.activity.main.IMAGE_TRANSITION_NAME_KEY


class DetailsActivity : PresentationActivity<DetailsView>(), DetailsView {

    @Inject
    lateinit var detailsActivityPresenter: DetailsActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setPresenter(detailsActivityPresenter, this)
        supportPostponeEnterTransition()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val transitionName = intent.extras.getString(IMAGE_TRANSITION_NAME_KEY)
            avatar.transitionName = transitionName
        }
    }

    override fun showGithubRepositoryDetails(repositoryName: String?, repositoryDescription: String?, ownerName: String?, avatarUrl: String?) {
        fillLayout(repositoryName, repositoryDescription, ownerName, avatarUrl)
        bitbucket_image.visibility = View.GONE
    }

    override fun showBitbucketRepositoryDetails(repositoryName: String?, repositoryDescription: String?, ownerName: String?, avatarUrl: String?) {
        fillLayout(repositoryName, repositoryDescription, ownerName, avatarUrl)
        bitbucket_image.visibility = View.VISIBLE
    }

    private fun fillLayout(repositoryName: String?, repositoryDescription: String?, ownerName: String?, avatarUrl: String?) {
        details_repository_name.text = String.format(getString(R.string.repository_name), repositoryName)
        details_repository_owner.text = String.format(getString(R.string.owner_field_text), ownerName)
        description.text = String.format(getString(R.string.description_text_field), repositoryDescription)
        loadImage(avatarUrl)
    }

    private fun loadImage(avatarUrl: String?) {
        val requestOptions = RequestOptions()
                .dontAnimate()
                .dontTransform()
                .override(300, 300)
                .fitCenter()

        Glide.with(baseContext)
                .applyDefaultRequestOptions(requestOptions)
                .load(avatarUrl)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        supportStartPostponedEnterTransition()
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        supportStartPostponedEnterTransition()
                        return false
                    }
                }).into(avatar)
    }

    override fun injectComponent() {
        APZUMIApplication.component
                .inject(this)
    }
}