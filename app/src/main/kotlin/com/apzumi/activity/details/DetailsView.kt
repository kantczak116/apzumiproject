package com.apzumi.activity.details

import com.core.mvp.View

interface DetailsView : View {

    fun showGithubRepositoryDetails(repositoryName: String?, repositoryDescription: String?, ownerName: String?, avatarUrl: String?)

    fun showBitbucketRepositoryDetails(repositoryName: String?, repositoryDescription: String?, ownerName: String?, avatarUrl: String?)
}