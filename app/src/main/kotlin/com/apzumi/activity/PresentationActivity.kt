package com.apzumi.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.core.mvp.Presenter
import com.core.mvp.View

abstract class PresentationActivity<V : View> : AppCompatActivity() {

    private var presenter: Presenter<V>? = null
    private var view: V? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectComponent()
    }

    protected abstract fun injectComponent()

    protected fun setPresenter(presenter: Presenter<V>, view: V) {
        this.presenter = presenter
        this.view = view
    }

    override fun onStart() {
        super.onStart()
        view?.let {
            presenter?.attachView(it)
        }
    }

    override fun onStop() {
        presenter?.detachView()
        super.onStop()
    }

}