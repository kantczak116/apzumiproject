package com.apzumi.di.modules

import com.apzumi.presenter.MainActivityPresenter
import com.core.data.provider.RepositoryItemDataProvider
import com.core.network.BitbucketService
import com.core.network.GithubService
import com.core.network.okhttp.HttpClientProvider
import com.core.network.okhttp.Timeout
import com.core.network.retrofit.repository.ApiBitbucketRepository
import com.core.network.retrofit.repository.ApiGithubRepository
import com.core.network.retrofit.service.HttpServiceProvider
import com.core.network.retrofit.service.RetrofitServiceProvider
import com.core.utils.RepositoryComparator
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val CONNECTION_TIMEOUT_SECONDS = 3L
private const val GITHUB_URL = "https://api.github.com/"
private const val BITBUCKET_URL = "https://api.bitbucket.org/2.0/repositories/"

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideRepositoryItemDataProvider(): RepositoryItemDataProvider = RepositoryItemDataProvider()

    @Singleton
    @Provides
    fun provideHttpClient(): HttpClientProvider = HttpClientProvider(Timeout(CONNECTION_TIMEOUT_SECONDS, TimeUnit.SECONDS))

    @Singleton
    @Provides
    fun provideGithub(httpClientProvider: HttpClientProvider): GithubService {
        val serviceProvider = getGithubServiceProvider(httpClientProvider)
        return ApiGithubRepository(serviceProvider)
    }

    private fun getGithubServiceProvider(httpClientProvider: HttpClientProvider): HttpServiceProvider {
        val httpClient: OkHttpClient = httpClientProvider.httpClient
        return RetrofitServiceProvider(GITHUB_URL, httpClient)
    }

    @Singleton
    @Provides
    fun provideBitbucket(httpClientProvider: HttpClientProvider): BitbucketService {
        val serviceProvider = getBitbucketServiceProvider(httpClientProvider)
        return ApiBitbucketRepository(serviceProvider)
    }

    private fun getBitbucketServiceProvider(httpClientProvider: HttpClientProvider): HttpServiceProvider {
        val httpclient: OkHttpClient = httpClientProvider.httpClient
        return RetrofitServiceProvider(BITBUCKET_URL, httpclient)
    }

    @Singleton
    @Provides
    fun provideMainActivityPresenter(githubService: GithubService,
                                     bitbucketService: BitbucketService,
                                     repositoryItemDataProvider: RepositoryItemDataProvider): MainActivityPresenter {
        val repositoryComparator = RepositoryComparator()
        return MainActivityPresenter(githubService, bitbucketService, repositoryItemDataProvider, repositoryComparator)
    }
}