package com.apzumi.di.component

import com.apzumi.activity.details.DetailsActivity
import com.apzumi.activity.main.MainActivity
import dagger.Component
import com.apzumi.di.modules.AppModule
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(mainActivity: MainActivity)

    fun inject(mainActivity: DetailsActivity)

}