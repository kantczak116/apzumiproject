package com.apzumi.presenter

import com.apzumi.activity.details.DetailsView
import com.core.data.provider.RepositoryItemDataProvider
import com.core.data.repository.Repository
import com.core.mvp.BasePresenter
import javax.inject.Inject

class DetailsActivityPresenter @Inject constructor(private val repositoryItemDataProvider: RepositoryItemDataProvider) : BasePresenter<DetailsView>() {

    override fun attachView(view: DetailsView) {
        super.attachView(view)
        showRepositoryDetails(view)
    }

    private fun showRepositoryDetails(view: DetailsView) {
        val repositoryItem = repositoryItemDataProvider.item
        when (repositoryItem) {
            is Repository.BitbucketRepository -> showBitbucketRepositoryDetails(view, repositoryItem)
            is Repository.GithubRepository -> showGithubRepositoryDetails(view, repositoryItem)
        }
    }

    private fun showGithubRepositoryDetails(view: DetailsView, repositoryItem: Repository.GithubRepository) {
        val owner = repositoryItem.owner
        view.showGithubRepositoryDetails(repositoryItem.name, repositoryItem.description, owner.login, owner.avatarUrl)
    }

    private fun showBitbucketRepositoryDetails(view: DetailsView, repositoryItem: Repository.BitbucketRepository) {
        val owner = repositoryItem.owner
        val ownerName = StringBuilder().append(owner.displayName)
                .append("\n")
                .append(owner.username)
                .toString()
        view.showBitbucketRepositoryDetails(repositoryItem.name, repositoryItem.description, ownerName, owner.links.avatar.href)
    }
}