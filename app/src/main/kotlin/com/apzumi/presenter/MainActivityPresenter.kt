package com.apzumi.presenter

import com.apzumi.activity.main.MainView
import com.core.data.provider.RepositoryItemDataProvider
import com.core.data.repository.Repository
import com.core.mvp.BasePresenter
import com.core.network.BitbucketService
import com.core.network.GithubService
import com.core.utils.RepositoryComparator
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class MainActivityPresenter(private val github: GithubService,
                            private val bitbucket: BitbucketService,
                            private val repositoryItemDataProvider: RepositoryItemDataProvider,
                            private val repositoryComparator: RepositoryComparator) : BasePresenter<MainView>() {

    private var repositoryList: List<Repository>? = null
    private var isSorted = false

    override fun startPresenting() {
        fetchRepositories()
    }

    fun fetchRepositories() {
        repositoryList?.let {
            when (isSorted) {
                true -> showSortedList()
                false -> showRestoredList()
            }
        } ?: getRepositoriesFromServer()
    }

    private fun getRepositoriesFromServer() {
        getGithubRepositories()
                .zipWith(getBitbucketRepositories(), joinToOneList())
                .doOnSubscribe { view?.showProgressBar() }
                .doOnError { view?.hideProgressBar() }
                .doOnSuccess {
                    view?.hideProgressBar()
                    repositoryList = it
                }
                .subscribe({ onRepositoryListFetched(it) }, { onError(it) })
    }

    private fun joinToOneList(): BiFunction<List<Repository>, List<Repository>, List<Repository>> {
        return BiFunction { githubRepositoriesList: List<Repository>, bitbucketRepositoriesList: List<Repository> ->
            ArrayList<Repository>().also {
                it.addAll(githubRepositoriesList)
                it.addAll(bitbucketRepositoriesList)
            }.toList()
        }
    }

    private fun getBitbucketRepositories(): Single<List<Repository>> {
        return bitbucket.repositories
                .map { it.repositoryList.map { it as Repository } }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun getGithubRepositories(): Single<List<Repository>> {
        return github.repositories
                .map { it.map { it as Repository } }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun onRepositoryListFetched(repositoryList: List<Repository>) {
        view?.showRepositoryList(repositoryList)
    }

    private fun onError(throwable: Throwable) {
        throwable.printStackTrace()
        view?.showError()
    }

    fun onRepositoryItemClicked(item: Repository) {
        repositoryItemDataProvider.item = item
    }

    fun onSortCheckboxClicked(checked: Boolean) {
        when (checked) {
            true -> showSortedList()
            false -> showRestoredList()
        }
    }

    private fun showSortedList() {
        isSorted = true
        repositoryList?.let {
            val sortedList = ArrayList<Repository>().apply {
                addAll(it)
            }
            repositoryComparator.compare(sortedList)
            view?.showSortedRepositoryList(sortedList)
        }
    }

    private fun showRestoredList() {
        repositoryList?.let {
            isSorted = false
            view?.showRepositoryList(it)
        }
    }
}