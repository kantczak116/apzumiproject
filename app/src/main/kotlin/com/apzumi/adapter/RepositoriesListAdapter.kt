package com.apzumi.adapter

import android.content.Context
import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.apzumi.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.core.data.repository.Repository
import com.core.data.repository.RepositoryType


class RepositoriesListAdapter(private val context: Context,
                              private val repositories: ArrayList<Repository>,
                              private val onClickListener: OnRepositoryItemClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            RepositoryType.BITBUCKET -> createBitBucketViewHolder(parent)
            RepositoryType.GITHUB -> createGithubViewHolder(parent)
            else -> throw IllegalStateException("View type not supported")
        }
    }

    private fun createBitBucketViewHolder(parent: ViewGroup): BitbucketViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.bitbucket_repositories_list_element, parent, false)
        return BitbucketViewHolder(view, onClickListener)
    }

    private fun createGithubViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.github_repositories_list_element, parent, false)
        return GithubViewHolder(view, onClickListener)
    }

    override fun getItemCount(): Int {
        return repositories.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemType = getItemViewType(position)
        when (itemType) {
            RepositoryType.BITBUCKET -> bindBitbucketViewHolder(holder, position)
            RepositoryType.GITHUB -> bindGithubViewHolder(holder, position)
        }
    }

    private fun bindBitbucketViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as BitbucketViewHolder).let {
            ViewCompat.setTransitionName(it.avatar, position.toString())
            val repositoryItem = repositories[position] as Repository.BitbucketRepository
            it.item = repositoryItem
            val ownerName = StringBuilder().append(repositoryItem.owner.displayName)
                    .append(" ")
                    .append(repositoryItem.owner.username)
            it.ownerName.text = String.format(context.getString(R.string.owner_field_text), ownerName)
            it.repositoryName.text = String.format(context.getString(R.string.repository_name), repositoryItem.name)
            loadAvatar(repositoryItem.owner.links.avatar.href, it.avatar)
        }
    }

    private fun bindGithubViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as GithubViewHolder).let {
            ViewCompat.setTransitionName(it.avatar, position.toString())
            val repositoryItem = repositories[position] as Repository.GithubRepository
            it.item = repositoryItem
            it.ownerName.text = String.format(context.getString(R.string.owner_field_text), repositoryItem.owner.login)
            it.repositoryName.text = String.format(context.getString(R.string.repository_name), repositoryItem.name)
            loadAvatar(repositoryItem.owner.avatarUrl, it.avatar)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (repositories[position]) {
            is Repository.BitbucketRepository -> RepositoryType.BITBUCKET
            is Repository.GithubRepository -> RepositoryType.GITHUB
        }
    }

    private fun loadAvatar(avatarUrl: String, imageView: ImageView) {
        val requestOptions = RequestOptions()
                .override(300, 300)
                .placeholder(R.drawable.placeholder)
                .centerCrop()

        Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(avatarUrl)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView)
    }

    fun setList(repositoryList: List<Repository>) {
        repositories.let {
            it.clear()
            it.addAll(repositoryList)
        }
        notifyDataSetChanged()
    }

    private class BitbucketViewHolder(view: View,
                                      onClickListener: OnRepositoryItemClickListener) : RecyclerView.ViewHolder(view) {

        lateinit var item: Repository
        val ownerName: TextView = view.findViewById(R.id.owner)
        val repositoryName: TextView = view.findViewById(R.id.repository_name)
        val avatar: ImageView = view.findViewById(R.id.owner_avatar)

        init {
            view.setOnClickListener { onClickListener.onClick(avatar, item) }
        }
    }

    private class GithubViewHolder(view: View,
                                   onCLickListener: OnRepositoryItemClickListener) : RecyclerView.ViewHolder(view) {

        lateinit var item: Repository
        val ownerName: TextView = view.findViewById(R.id.github_owner)
        val repositoryName: TextView = view.findViewById(R.id.github_repository_name)
        val avatar: ImageView = view.findViewById(R.id.github_owner_avatar)

        init {
            view.setOnClickListener { onCLickListener.onClick(avatar, item) }
        }
    }
}