package com.apzumi.adapter

import android.widget.ImageView
import com.core.data.repository.Repository

interface OnRepositoryItemClickListener {

    fun onClick(imageView: ImageView, item: Repository)
}